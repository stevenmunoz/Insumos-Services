USE [Insumos]
GO
ALTER TABLE [dbo].[Tipo_Producto] DROP CONSTRAINT [FK_Tipo_Producto_Tipo_Insumo]
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio] DROP CONSTRAINT [FK_Reporte_Variacion_Precio_Punto_Venta]
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio] DROP CONSTRAINT [FK_Reporte_Variacion_Precio_Municipio]
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio] DROP CONSTRAINT [FK_Reporte_Variacion_Precio_Insumo]
GO
ALTER TABLE [dbo].[Punto_Venta_Insumos] DROP CONSTRAINT [FK_Punto_Venta_Insumos_Punto_Venta]
GO
ALTER TABLE [dbo].[Punto_Venta_Insumos] DROP CONSTRAINT [FK_Punto_Venta_Insumos_Insumo]
GO
ALTER TABLE [dbo].[Punto_Venta] DROP CONSTRAINT [FK_Punto_Venta_Municipio]
GO
ALTER TABLE [dbo].[Municipio] DROP CONSTRAINT [FK_Municipio_Departamento]
GO
ALTER TABLE [dbo].[Insumo_Municipio_Precio] DROP CONSTRAINT [FK_Insumo_Municipio_Precio_Municipio]
GO
ALTER TABLE [dbo].[Insumo_Municipio_Precio] DROP CONSTRAINT [FK_Insumo_Municipio_Precio_Insumo]
GO
ALTER TABLE [dbo].[Insumo_Departamento_Precio] DROP CONSTRAINT [FK_Insumo_Departamento_Precio_Insumo]
GO
ALTER TABLE [dbo].[Insumo_Departamento_Precio] DROP CONSTRAINT [FK_Insumo_Departamento_Precio_Departamento]
GO
ALTER TABLE [dbo].[Insumo] DROP CONSTRAINT [FK_Insumo_Tipo_Producto]
GO
ALTER TABLE [dbo].[Calificacion] DROP CONSTRAINT [FK_Calificacion_Punto_Venta]
GO
/****** Object:  Table [dbo].[Tipo_Producto]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Tipo_Producto]
GO
/****** Object:  Table [dbo].[Tipo_Insumo]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Tipo_Insumo]
GO
/****** Object:  Table [dbo].[Reporte_Variacion_Precio]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Reporte_Variacion_Precio]
GO
/****** Object:  Table [dbo].[Punto_Venta_Insumos]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Punto_Venta_Insumos]
GO
/****** Object:  Table [dbo].[Punto_Venta]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Punto_Venta]
GO
/****** Object:  Table [dbo].[Municipio]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Municipio]
GO
/****** Object:  Table [dbo].[Insumo_Municipio_Precio]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Insumo_Municipio_Precio]
GO
/****** Object:  Table [dbo].[Insumo_Departamento_Precio]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Insumo_Departamento_Precio]
GO
/****** Object:  Table [dbo].[Insumo]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Insumo]
GO
/****** Object:  Table [dbo].[Departamento]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Departamento]
GO
/****** Object:  Table [dbo].[Consejo]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Consejo]
GO
/****** Object:  Table [dbo].[Calificacion]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Calificacion]
GO
/****** Object:  Table [dbo].[Boletin]    Script Date: 22/08/2014 9:06:39 a. m. ******/
DROP TABLE [dbo].[Boletin]
GO
/****** Object:  Table [dbo].[Boletin]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Boletin](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nvarchar](128) NOT NULL,
	[imagen] [nvarchar](1024) NOT NULL,
	[url] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Boletin] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Calificacion]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calificacion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_punto_venta] [int] NOT NULL,
	[calificacion] [int] NOT NULL,
 CONSTRAINT [PK_Calificacion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Consejo]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Consejo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nvarchar](128) NOT NULL,
	[descripcion] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_Consejo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Departamento]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departamento](
	[id] [int] NOT NULL,
	[nombre] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Departamento] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Insumo]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Insumo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cantidad_presentacion] [int] NOT NULL,
	[unidad_presentacion] [nvarchar](20) NOT NULL,
	[precio_ministerio] [float] NOT NULL,
	[precio_promedio_reportado] [float] NULL,
	[id_tipo_producto] [int] NOT NULL,
	[tipo_produccion] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_Insumo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Insumo_Departamento_Precio]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Insumo_Departamento_Precio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[precio_departamento] [float] NOT NULL,
	[id_departamento] [int] NOT NULL,
	[id_insumo] [int] NOT NULL,
 CONSTRAINT [PK_Insumo_Departamento_Precio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Insumo_Municipio_Precio]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Insumo_Municipio_Precio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_municipio] [int] NOT NULL,
	[id_departaimento] [int] NOT NULL,
	[id_insumo] [int] NOT NULL,
	[precio_municipal] [float] NULL,
 CONSTRAINT [PK_Insumo_Municipio_Precio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Municipio]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Municipio](
	[id] [int] NOT NULL,
	[id_departamento] [int] NOT NULL,
	[nombre] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Municipio] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_departamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Punto_Venta]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Punto_Venta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_municipio] [int] NOT NULL,
	[id_departamento] [int] NOT NULL,
	[nombre] [nvarchar](128) NOT NULL,
	[direccion] [nvarchar](128) NOT NULL,
	[telefono] [nvarchar](50) NOT NULL,
	[promedio_calificacion] [float] NULL,
	[latitud] [float] NULL,
	[longitud] [float] NULL,
 CONSTRAINT [PK_Punto_Venta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Punto_Venta_Insumos]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Punto_Venta_Insumos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_punto_venta] [int] NOT NULL,
	[id_insumo] [int] NOT NULL,
 CONSTRAINT [PK_Punto_Venta_Tipo_Producto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reporte_Variacion_Precio]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reporte_Variacion_Precio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_punto_venta] [int] NULL,
	[id_insumo] [int] NOT NULL,
	[id_municipio] [int] NOT NULL,
	[id_departamento] [int] NOT NULL,
	[precio] [float] NOT NULL,
	[punto_venta] [nvarchar](128) NULL,
 CONSTRAINT [PK_Reporte_Variacion_Precio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tipo_Insumo]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Insumo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Tipo_Insumo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tipo_Producto]    Script Date: 22/08/2014 9:06:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Producto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](128) NOT NULL,
	[id_tipo_insumo] [int] NOT NULL,
 CONSTRAINT [PK_Tipo_Producto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Boletin] ON 

GO
INSERT [dbo].[Boletin] ([id], [titulo], [imagen], [url]) VALUES (1, N'MinAgricultura presenta el balance de gestión 2013- 2014', N'http://img.eltiempo.com/contenido/economia/sectores/IMAGEN/IMAGEN-14367636-2.jpg', N'https://www.minagricultura.gov.co/noticias/Paginas/MinAgricultura-presenta-el-balance.aspx/')
GO
SET IDENTITY_INSERT [dbo].[Boletin] OFF
GO
SET IDENTITY_INSERT [dbo].[Calificacion] ON 

GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (1, 1, 3)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (2, 1, 4)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (3, 2, 5)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (4, 2, 1)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (5, 1, 1)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (6, 2, 2)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (7, 1, 3)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (8, 2, 4)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (9, 1, 5)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (10, 2, 1)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (11, 1, 5)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (12, 1, 5)
GO
INSERT [dbo].[Calificacion] ([id], [id_punto_venta], [calificacion]) VALUES (13, 3, 1)
GO
SET IDENTITY_INSERT [dbo].[Calificacion] OFF
GO
SET IDENTITY_INSERT [dbo].[Consejo] ON 

GO
INSERT [dbo].[Consejo] ([id], [titulo], [descripcion]) VALUES (1, N'Cómo preparar la tierra ideal para tus cultivos en casa', N'Lo primordial: la tierra ¿Cómo ha de ser la tierra de tu huerto urbano? Tanto si cultivas directamente en suelo como si lo haces en una mesa de cultivo, la tierra debe ser fértil y con un buen drenaje… sin olvidar sus nutrientes. Cultivar en macetas o mesas de cultivo provoca un mayor agotamiento de la tierra, es por ello que se recomienda utilizar abono orgánico vegetal (compost, humus de gusanos) o abono de estiércol de ganado (evitar fertilizantes químicos).')
GO
SET IDENTITY_INSERT [dbo].[Consejo] OFF
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (5, N'ANTIOQUIA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (8, N'ATLÁNTICO')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (11, N'BOGOTÁ D.C')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (13, N'BOLÍVAR')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (15, N'BOYACÁ')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (17, N'CALDAS')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (18, N'CAQUETÁ')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (19, N'CAUCA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (20, N'CESAR')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (23, N'CÓRDOBA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (25, N'CUNDINAMARCA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (27, N'CHOCÓ')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (41, N'HUILA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (44, N'LA GUAJIRA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (47, N'MAGDALENA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (50, N'META')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (52, N'NARIÑO')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (54, N'NORTE DE SANTANDER')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (63, N'QUINDIO')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (66, N'RISARALDA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (68, N'SANTANDER')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (70, N'SUCRE')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (73, N'TOLIMA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (76, N'VALLE DEL CAUCA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (81, N'ARAUCA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (85, N'CASANARE')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (86, N'PUTUMAYO')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (88, N'ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (91, N'AMAZONAS')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (94, N'GUAINÍA')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (95, N'GUAVIARE')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (97, N'VAUPÉS')
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (99, N'VICHADA')
GO
SET IDENTITY_INSERT [dbo].[Insumo] ON 

GO
INSERT [dbo].[Insumo] ([id], [cantidad_presentacion], [unidad_presentacion], [precio_ministerio], [precio_promedio_reportado], [id_tipo_producto], [tipo_produccion]) VALUES (1, 50, N'Kilogramo', 67104.18, NULL, 1, N'COMPLEJO')
GO
INSERT [dbo].[Insumo] ([id], [cantidad_presentacion], [unidad_presentacion], [precio_ministerio], [precio_promedio_reportado], [id_tipo_producto], [tipo_produccion]) VALUES (2, 50, N'Kilogramo', 68050.25, NULL, 2, N'COMPLEJO')
GO
INSERT [dbo].[Insumo] ([id], [cantidad_presentacion], [unidad_presentacion], [precio_ministerio], [precio_promedio_reportado], [id_tipo_producto], [tipo_produccion]) VALUES (3, 50, N'Kilogramo', 75490.64, NULL, 3, N'COMPLEJO')
GO
INSERT [dbo].[Insumo] ([id], [cantidad_presentacion], [unidad_presentacion], [precio_ministerio], [precio_promedio_reportado], [id_tipo_producto], [tipo_produccion]) VALUES (4, 50, N'Kilogramo', 51488.88, NULL, 4, N'NO APLICA')
GO
INSERT [dbo].[Insumo] ([id], [cantidad_presentacion], [unidad_presentacion], [precio_ministerio], [precio_promedio_reportado], [id_tipo_producto], [tipo_produccion]) VALUES (5, 50, N'Kilogramo', 66601.51, NULL, 5, N'NO APLICA')
GO
SET IDENTITY_INSERT [dbo].[Insumo] OFF
GO
SET IDENTITY_INSERT [dbo].[Insumo_Departamento_Precio] ON 

GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (1, 51727.07, 5, 4)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (2, 67992.19, 5, 5)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (3, 68225.65, 5, 1)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (4, 67462.55, 5, 2)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (5, 75120.26, 5, 3)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (6, 49530.63, 17, 4)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (7, 66349.06, 17, 5)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (8, 65598.66, 17, 1)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (9, 65956.98, 17, 2)
GO
INSERT [dbo].[Insumo_Departamento_Precio] ([id], [precio_departamento], [id_departamento], [id_insumo]) VALUES (10, 70033.33, 17, 3)
GO
SET IDENTITY_INSERT [dbo].[Insumo_Departamento_Precio] OFF
GO
SET IDENTITY_INSERT [dbo].[Insumo_Municipio_Precio] ON 

GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (1, 2, 5, 4, 50039)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (2, 2, 5, 5, 67714)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (3, 2, 5, 1, 69362)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (4, 2, 5, 2, 68000)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (5, 2, 5, 3, 75700)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (6, 1, 17, 4, 52300)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (7, 1, 17, 5, 69450)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (8, 1, 17, 1, 66900)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (9, 1, 17, 2, 66480)
GO
INSERT [dbo].[Insumo_Municipio_Precio] ([id], [id_municipio], [id_departaimento], [id_insumo], [precio_municipal]) VALUES (10, 1, 17, 3, 75700)
GO
SET IDENTITY_INSERT [dbo].[Insumo_Municipio_Precio] OFF
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 5, N'MEDELLÍN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 8, N'BARRANQUILLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 11, N'BOGOTÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 13, N'CARTAGENA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 15, N'TUNJA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 17, N'MANIZALES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 18, N'FLORENCIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 19, N'POPAYÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 20, N'VALLEDUPAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 23, N'MONTERÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 25, N'AGUA DE DIOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 27, N'QUIBDÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 41, N'NEIVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 44, N'RIOHACHA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 47, N'SANTA MARTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 50, N'VILLAVICENCIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 52, N'PASTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 54, N'CÚCUTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 63, N'ARMENIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 66, N'PEREIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 68, N'BUCARAMANGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 70, N'SINCELEJO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 73, N'IBAGUÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 76, N'CALI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 81, N'ARAUCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 85, N'YOPAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 86, N'MOCOA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 88, N'SAN ANDRÉS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 91, N'LETICIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 94, N'INÍRIDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 95, N'SAN JOSÉ DEL GUAVIARE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 97, N'MITÚ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (1, 99, N'PUERTO CARREÑO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (2, 5, N'ABEJORRAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (3, 54, N'ABREGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (4, 5, N'ABRIAQUÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (6, 13, N'ACHÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (6, 27, N'ACANDÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (6, 41, N'ACEVEDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (6, 50, N'ACACÍAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (10, 85, N'AGUAZUL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (11, 20, N'AGUACHICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (13, 17, N'AGUADAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (13, 20, N'AGUSTÍN CODAZZI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (13, 41, N'AGRADO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (13, 68, N'AGUADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (15, 85, N'CHAMEZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (15, 95, N'CALAMAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (16, 41, N'AIPE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (19, 25, N'ALBÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (19, 52, N'ALBÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (20, 41, N'ALGECIRAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (20, 68, N'ALBANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (20, 76, N'ALCALÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (21, 5, N'ALEJANDRÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (22, 15, N'ALMEIDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (22, 19, N'ALMAGUER')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (22, 52, N'ALDANA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (24, 73, N'ALPUJARRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (25, 27, N'ALTO BAUDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (25, 95, N'EL RETORNO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (26, 41, N'ALTAMIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (26, 73, N'ALVARADO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (29, 18, N'ALBANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (30, 5, N'AMAGÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (30, 13, N'ALTOS DEL ROSARIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (30, 47, N'ALGARROBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (30, 73, N'AMBALEMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (31, 5, N'AMALFI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (32, 20, N'ASTREA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (34, 5, N'ANDES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (35, 25, N'ANAPOIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (35, 44, N'ALBANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (36, 5, N'ANGELÓPOLIS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (36, 52, N'ANCUYÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (36, 76, N'ANDALUCÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (38, 5, N'ANGOSTURA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (40, 5, N'ANORÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (40, 25, N'ANOLAIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (41, 76, N'ANSERMANUEVO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (42, 5, N'SANTAFÉ DE ANTIOQUIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (42, 13, N'ARENAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (42, 17, N'ANSERMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (43, 73, N'ANZOÁTEGUI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (44, 5, N'ANZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (45, 5, N'APARTADÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (45, 20, N'BECERRIL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (45, 66, N'APÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (47, 15, N'AQUITANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (50, 17, N'ARANZAZU')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (50, 19, N'ARGELIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (50, 27, N'ATRATO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (51, 5, N'ARBOLETES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (51, 15, N'ARCABUCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (51, 52, N'ARBOLEDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (51, 54, N'ARBOLEDAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (51, 68, N'ARATOCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (52, 13, N'ARJONA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (53, 25, N'ARBELÁEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (53, 47, N'ARACATACA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (54, 76, N'ARGELIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (55, 5, N'ARGELIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (55, 73, N'ARMERO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (58, 47, N'ARIGUANÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (59, 5, N'ARMENIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (60, 20, N'BOSCONIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (62, 13, N'ARROYOHONDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (65, 81, N'ARAUQUITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (67, 73, N'ATACO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (68, 23, N'AYAPEL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (73, 27, N'BAGADÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (74, 13, N'BARRANCO DE LOBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (75, 19, N'BALBOA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (75, 27, N'BAHÍA SOLANO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (75, 66, N'BALBOA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (77, 27, N'BAJO BAUDÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (77, 68, N'BARBOSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (78, 8, N'BARANOA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (78, 41, N'BARAYA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (78, 44, N'BARRANCAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (79, 5, N'BARBOSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (79, 23, N'BUENAVISTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (79, 52, N'BARBACOAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (79, 68, N'BARICHARA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (81, 68, N'BARRANCABERMEJA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (83, 52, N'BELÉN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (86, 5, N'BELMIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (86, 25, N'BELTRÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (86, 27, N'BELÉN DE BAJIRÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (87, 15, N'BELÉN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (88, 5, N'BELLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (88, 17, N'BELALCÁZAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (88, 66, N'BELÉN DE UMBRÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (90, 15, N'BERBEO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (90, 23, N'CANALETE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (90, 44, N'DIBULLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (91, 5, N'BETANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (92, 15, N'BETÉITIVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (92, 68, N'BETULIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (93, 5, N'BETULIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (94, 18, N'BELÉN DE LOS ANDAQUIES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (95, 25, N'BITUIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (97, 15, N'BOAVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (98, 44, N'DISTRACCIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (99, 25, N'BOJACÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (99, 27, N'BOJAYA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (99, 54, N'BOCHALEMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (100, 19, N'BOLÍVAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (100, 76, N'BOLÍVAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (101, 5, N'CIUDAD BOLÍVAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (101, 68, N'BOLÍVAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (104, 15, N'BOYACÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (106, 15, N'BRICEÑO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (107, 5, N'BRICEÑO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (109, 15, N'BUENAVISTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (109, 54, N'BUCARASICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (109, 76, N'BUENAVENTURA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (110, 19, N'BUENOS AIRES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (110, 44, N'EL MOLINO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (110, 50, N'BARRANCA DE UPÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (110, 52, N'BUESACO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (110, 70, N'BUENAVISTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (111, 63, N'BUENAVISTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (111, 76, N'GUADALAJARA DE BUGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (113, 5, N'BURITICÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (113, 76, N'BUGALAGRANDE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (114, 15, N'BUSBANZÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (120, 5, N'CÁCERES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (120, 25, N'CABRERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (121, 68, N'CABRERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (122, 76, N'CAICEDONIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (123, 25, N'CACHIPAY')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (124, 50, N'CABUYARO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (124, 70, N'CAIMITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (124, 73, N'CAJAMARCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (125, 5, N'CAICEDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (125, 54, N'CÁCOTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (125, 85, N'HATO COROZAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (126, 25, N'CAJICÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (126, 76, N'CALIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (128, 54, N'CACHIRÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (129, 5, N'CALDAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (130, 19, N'CAJIBÍO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (130, 63, N'CALARCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (130, 76, N'CANDELARIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (131, 15, N'CALDAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (132, 41, N'CAMPOALEGRE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (132, 68, N'CALIFORNIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (134, 5, N'CAMPAMENTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (135, 15, N'CAMPOHERMOSO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (135, 27, N'EL CANTÓN DEL SAN PABLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (136, 85, N'LA SALINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (137, 8, N'CAMPO DE LA CRUZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (137, 19, N'CALDONO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (138, 5, N'CAÑASGORDAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (139, 85, N'MANÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (140, 13, N'CALAMAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (141, 8, N'CANDELARIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (142, 5, N'CARACOLÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (142, 19, N'CALOTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (145, 5, N'CARAMANTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (147, 5, N'CAREPA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (147, 68, N'CAPITANEJO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (147, 76, N'CARTAGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (148, 5, N'EL CARMEN DE VIBORAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (148, 25, N'CAPARRAPÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (148, 73, N'CARMEN DE APICALÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (150, 5, N'CAROLINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (150, 18, N'CARTAGENA DEL CHAIRÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (150, 27, N'CARMEN DEL DARIEN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (150, 50, N'CASTILLA LA NUEVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (151, 25, N'CAQUEZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (152, 68, N'CARCASÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (152, 73, N'CASABIANCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (154, 5, N'CAUCASIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (154, 25, N'CARMEN DE CARUPA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (160, 13, N'CANTAGALLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (160, 27, N'CÉRTEGUI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (160, 68, N'CEPITÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (161, 47, N'CERRO SAN ANTONIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (161, 97, N'CARURU')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (162, 15, N'CERINZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (162, 23, N'CERETÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (162, 68, N'CERRITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (162, 85, N'MONTERREY')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (167, 68, N'CHARALÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (168, 23, N'CHIMÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (168, 25, N'CHAGUANÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (168, 73, N'CHAPARRAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (169, 68, N'CHARTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (170, 47, N'CHIBOLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (170, 66, N'DOSQUEBRADAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (172, 5, N'CHIGORODÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (172, 15, N'CHINAVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (172, 54, N'CHINÁCOTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (174, 17, N'CHINCHINÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (174, 54, N'CHITAGÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (175, 20, N'CHIMICHAGUA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (175, 25, N'CHÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (176, 15, N'CHIQUINQUIRÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (176, 68, N'CHIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (178, 20, N'CHIRIGUANÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (178, 25, N'CHIPAQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (179, 68, N'CHIPATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (180, 15, N'CHISCAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (181, 25, N'CHOACHÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (182, 23, N'CHINÚ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (183, 15, N'CHITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (183, 25, N'CHOCONTÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (185, 15, N'CHITARAQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (187, 15, N'CHIVATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (188, 13, N'CICUCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (189, 15, N'CIÉNEGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (189, 23, N'CIÉNAGA DE ORO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (189, 47, N'CIÉNAGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (190, 5, N'CISNEROS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (190, 63, N'CIRCASIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (190, 68, N'CIMITARRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (197, 5, N'COCORNÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (200, 25, N'COGUA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (200, 73, N'COELLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (200, 95, N'MIRAFLORES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (203, 52, N'COLÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (204, 15, N'CÓMBITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (204, 70, N'COLOSO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (205, 18, N'CURILLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (205, 27, N'CONDOTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (205, 47, N'CONCORDIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (206, 5, N'CONCEPCIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (206, 41, N'COLOMBIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (206, 54, N'CONVENCIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (207, 52, N'CONSACA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (207, 68, N'CONCEPCIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (209, 5, N'CONCORDIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (209, 68, N'CONFINES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (210, 52, N'CONTADERO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (211, 68, N'CONTRATACIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (212, 5, N'COPACABANA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (212, 13, N'CÓRDOBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (212, 15, N'COPER')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (212, 19, N'CORINTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (212, 63, N'CÓRDOBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (214, 25, N'COTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (215, 15, N'CORRALES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (215, 52, N'CÓRDOBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (215, 70, N'COROZAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (217, 68, N'COROMORO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (217, 73, N'COYAIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (218, 15, N'COVARACHÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (219, 86, N'COLÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (220, 81, N'CRAVO NORTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (221, 70, N'COVEÑAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (222, 13, N'CLEMENCIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (223, 15, N'CUBARÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (223, 50, N'CUBARRAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (223, 54, N'CUCUTILLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (224, 15, N'CUCAITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (224, 25, N'CUCUNUBÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (224, 52, N'CUASPUD')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (225, 85, N'NUNCHÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (226, 15, N'CUÍTIVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (226, 50, N'CUMARAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (226, 73, N'CUNDAY')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (227, 52, N'CUMBAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (228, 20, N'CURUMANÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (229, 68, N'CURITÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (230, 70, N'CHALÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (230, 85, N'OROCUÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (232, 15, N'CHÍQUIZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (233, 52, N'CUMBITARA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (233, 70, N'EL ROBLE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (233, 76, N'DAGUA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (234, 5, N'DABEIBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (235, 68, N'EL CARMEN DE CHUCURÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (235, 70, N'GALERAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (236, 15, N'CHIVOR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (236, 73, N'DOLORES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (237, 5, N'DON MATÍAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (238, 15, N'DUITAMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (238, 20, N'EL COPEY')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (239, 54, N'DURANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (240, 5, N'EBÉJICO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (240, 52, N'CHACHAGÜÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (243, 76, N'EL ÁGUILA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (244, 13, N'EL CARMEN DE BOLÍVAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (244, 15, N'EL COCUY')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (244, 41, N'ELÍAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (245, 25, N'EL COLEGIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (245, 27, N'EL CARMEN DE ATRATO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (245, 47, N'EL BANCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (245, 50, N'EL CALVARIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (245, 54, N'EL CARMEN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (245, 68, N'EL GUACAMAYO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (246, 76, N'EL CAIRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (247, 18, N'EL DONCELLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (248, 13, N'EL GUAMO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (248, 15, N'EL ESPINO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (248, 76, N'EL CERRITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (250, 5, N'EL BAGRE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (250, 20, N'EL PASO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (250, 27, N'EL LITORAL DEL SAN JUAN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (250, 52, N'EL CHARCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (250, 54, N'EL TARRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (250, 68, N'EL PEÑÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (250, 76, N'EL DOVIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (250, 85, N'PAZ DE ARIPORO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (251, 50, N'EL CASTILLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (254, 52, N'EL PEÑOL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (255, 68, N'EL PLAYÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (256, 18, N'EL PAUJIL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (256, 19, N'EL TAMBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (256, 52, N'EL ROSARIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (258, 25, N'EL PEÑÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (258, 47, N'EL PIÑON')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (258, 52, N'EL TABLÓN DE GÓMEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (260, 25, N'EL ROSAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (260, 52, N'EL TAMBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (261, 54, N'EL ZULIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (263, 85, N'PORE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (263, 91, N'EL ENCANTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (264, 5, N'ENTRERRIOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (264, 68, N'ENCINO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (265, 70, N'GUARANDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (266, 5, N'ENVIGADO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (266, 68, N'ENCISO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (268, 13, N'EL PEÑÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (268, 47, N'EL RETÉN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (268, 73, N'ESPINAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (269, 25, N'FACATATIVÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (270, 50, N'EL DORADO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (270, 73, N'FALAN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (271, 68, N'FLORIÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (272, 15, N'FIRAVITOBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (272, 17, N'FILADELFIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (272, 63, N'FILANDIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (275, 73, N'FLANDES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (275, 76, N'FLORIDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (276, 15, N'FLORESTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (276, 68, N'FLORIDABLANCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (279, 25, N'FOMEQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (279, 44, N'FONSECA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (279, 85, N'RECETOR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (281, 25, N'FOSCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (282, 5, N'FREDONIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (283, 73, N'FRESNO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (284, 5, N'FRONTINO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (286, 25, N'FUNZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (287, 50, N'FUENTE DE ORO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (287, 52, N'FUNES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (288, 25, N'FÚQUENE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (288, 47, N'FUNDACIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (290, 19, N'FLORENCIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (290, 25, N'FUSAGASUGÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (293, 15, N'GACHANTIVÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (293, 25, N'GACHALA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (295, 20, N'GAMARRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (295, 25, N'GACHANCIPÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (296, 8, N'GALAPA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (296, 15, N'GAMEZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (296, 68, N'GALÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (297, 25, N'GACHETÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (298, 41, N'GARZÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (298, 68, N'GAMBITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (299, 15, N'GARAGOA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (299, 25, N'GAMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (300, 13, N'HATILLO DE LOBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (300, 23, N'COTORRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (300, 81, N'FORTUL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (300, 85, N'SABANALARGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (302, 63, N'GÉNOVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (306, 5, N'GIRALDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (306, 41, N'GIGANTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (306, 76, N'GINEBRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (307, 25, N'GIRARDOT')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (307, 68, N'GIRÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (308, 5, N'GIRARDOTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (310, 5, N'GÓMEZ PLATA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (310, 20, N'GONZÁLEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (312, 25, N'GRANADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (313, 5, N'GRANADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (313, 50, N'GRANADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (313, 54, N'GRAMALOTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (315, 5, N'GUADALUPE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (315, 85, N'SÁCAMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (317, 15, N'GUACAMAYAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (317, 25, N'GUACHETÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (317, 52, N'GUACHUCAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (318, 5, N'GUARNE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (318, 19, N'GUAPI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (318, 47, N'GUAMAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (318, 50, N'GUAMAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (318, 66, N'GUÁTICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (318, 68, N'GUACA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (318, 76, N'GUACARÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (319, 41, N'GUADALUPE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (319, 73, N'GUAMO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (320, 25, N'GUADUAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (320, 52, N'GUAITARILLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (320, 68, N'GUADALUPE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (320, 86, N'ORITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (321, 5, N'GUATAPE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (322, 15, N'GUATEQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (322, 25, N'GUASCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (322, 68, N'GUAPOTÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (323, 52, N'GUALMATÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (324, 25, N'GUATAQUÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (324, 68, N'GUAVATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (325, 15, N'GUAYATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (325, 50, N'MAPIRIPÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (325, 85, N'SAN LUIS DE PALENQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (326, 25, N'GUATAVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (327, 68, N'GÜEPSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (328, 25, N'GUAYABAL DE SIQUIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (330, 50, N'MESETAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (332, 15, N'GÜICÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (335, 25, N'GUAYABETAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (339, 25, N'GUTIÉRREZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (343, 94, N'BARRANCO MINAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (344, 54, N'HACARÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (344, 68, N'HATO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (347, 5, N'HELICONIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (347, 54, N'HERRÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (347, 73, N'HERVEO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (349, 41, N'HOBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (349, 73, N'HONDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (350, 23, N'LA APARTADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (350, 50, N'LA MACARENA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (352, 52, N'ILES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (352, 73, N'ICONONZO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (353, 5, N'HISPANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (354, 52, N'IMUÉS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (355, 19, N'INZÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (356, 52, N'IPIALES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (357, 41, N'IQUIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (359, 41, N'ISNOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (360, 5, N'ITAGUI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (361, 5, N'ITUANGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (361, 27, N'ISTMINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (362, 15, N'IZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (364, 5, N'JARDÍN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (364, 19, N'JAMBALÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (364, 76, N'JAMUNDÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (367, 15, N'JENESANO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (368, 5, N'JERICÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (368, 15, N'JERICÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (368, 25, N'JERUSALÉN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (368, 68, N'JESÚS MARÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (370, 50, N'URIBE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (370, 68, N'JORDÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (372, 8, N'JUAN DE ACOSTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (372, 25, N'JUNÍN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (372, 27, N'JURADÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (376, 5, N'LA CEJA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (377, 15, N'LABRANZAGRANDE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (377, 25, N'LA CALERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (377, 54, N'LABATECA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (377, 68, N'LA BELLEZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (377, 76, N'LA CUMBRE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (378, 41, N'LA ARGENTINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (378, 44, N'HATONUEVO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (378, 52, N'LA CRUZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (380, 5, N'LA ESTRELLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (380, 15, N'LA CAPILLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (380, 17, N'LA DORADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (381, 52, N'LA FLORIDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (383, 20, N'LA GLORIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (383, 66, N'LA CELIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (385, 52, N'LA LLANADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (385, 54, N'LA ESPERANZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (385, 68, N'LANDÁZURI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (386, 25, N'LA MESA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (388, 17, N'LA MERCED')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (390, 5, N'LA PINTADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (390, 52, N'LA TOLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (392, 19, N'LA SIERRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (394, 25, N'LA PALMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (396, 41, N'LA PLATA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (397, 19, N'LA VEGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (397, 68, N'LA PAZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (398, 25, N'LA PEÑA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (398, 54, N'LA PLAYA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (399, 52, N'LA UNIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (400, 5, N'LA UNIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (400, 20, N'LA JAGUA DE IBIRICO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (400, 50, N'LEJANÍAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (400, 66, N'LA VIRGINIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (400, 70, N'LA UNIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (400, 76, N'LA UNIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (400, 85, N'TÁMARA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (401, 15, N'LA VICTORIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (401, 63, N'LA TEBAIDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (402, 25, N'LA VEGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (403, 15, N'LA UVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (403, 76, N'LA VICTORIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (405, 52, N'LEIVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (405, 54, N'LOS PATIOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (405, 91, N'LA CHORRERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (406, 68, N'LEBRÍJA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (407, 15, N'VILLA DE LEYVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (407, 25, N'LENGUAZAQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (407, 91, N'LA PEDRERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (408, 73, N'LÉRIDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (410, 18, N'LA MONTAÑITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (410, 85, N'TAURAMENA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (411, 5, N'LIBORINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (411, 52, N'LINARES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (411, 73, N'LÍBANO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (413, 27, N'LLORÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (417, 23, N'LORICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (418, 19, N'LÓPEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (418, 52, N'LOS ANDES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (418, 54, N'LOURDES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (418, 68, N'LOS SANTOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (418, 70, N'LOS PALMITOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (419, 23, N'LOS CÓRDOBAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (420, 44, N'LA JAGUA DEL PILAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (421, 8, N'LURUACO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (425, 5, N'MACEO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (425, 15, N'MACANAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (425, 27, N'MEDIO ATRATO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (425, 68, N'MACARAVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (426, 25, N'MACHETA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (427, 52, N'MAGÜI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (429, 70, N'MAJAGUAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (430, 13, N'MAGANGUÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (430, 25, N'MADRID')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (430, 27, N'MEDIO BAUDÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (430, 44, N'MAICAO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (430, 85, N'TRINIDAD')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (430, 91, N'LA VICTORIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (432, 68, N'MÁLAGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (433, 8, N'MALAMBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (433, 13, N'MAHATES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (433, 17, N'MANZANARES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (435, 52, N'MALLAMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (436, 8, N'MANATÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (436, 25, N'MANTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (438, 25, N'MEDINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (440, 5, N'MARINILLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (440, 13, N'MARGARITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (440, 66, N'MARSELLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (440, 85, N'VILLANUEVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (442, 13, N'MARÍA LA BAJA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (442, 15, N'MARIPÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (442, 17, N'MARMATO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (443, 20, N'MANAURE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (443, 73, N'MARIQUITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (444, 17, N'MARQUETALIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (444, 68, N'MATANZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (446, 17, N'MARULANDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (449, 73, N'MELGAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (450, 19, N'MERCADERES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (450, 27, N'MEDIO SAN JUAN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (450, 50, N'PUERTO CONCORDIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (455, 15, N'MIRAFLORES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (455, 19, N'MIRANDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (456, 66, N'MISTRATÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (458, 13, N'MONTECRISTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (460, 18, N'MILÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (460, 47, N'NUEVA GRANADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (460, 91, N'MIRITI - PARANÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (461, 73, N'MURILLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (464, 15, N'MONGUA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (464, 23, N'MOMIL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (464, 68, N'MOGOTES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (466, 15, N'MONGUÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (466, 23, N'MONTELÍBANO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (467, 5, N'MONTEBELLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (468, 13, N'MOMPÓS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (468, 68, N'MOLAGAVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (469, 15, N'MONIQUIRÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (470, 63, N'MONTENEGRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (473, 13, N'MORALES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (473, 19, N'MORALES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (473, 25, N'MOSQUERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (473, 52, N'MOSQUERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (473, 70, N'MORROA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (475, 5, N'MURINDÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (476, 15, N'MOTAVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (479, 18, N'MORELIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (480, 5, N'MUTATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (480, 15, N'MUZO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (480, 52, N'NARIÑO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (480, 54, N'MUTISCUA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (483, 5, N'NARIÑO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (483, 25, N'NARIÑO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (483, 41, N'NÁTAGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (483, 73, N'NATAGAIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (486, 17, N'NEIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (486, 25, N'NEMOCÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (488, 25, N'NILO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (489, 25, N'NIMAIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (490, 5, N'NECOCLÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (490, 52, N'OLAYA HERRERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (491, 15, N'NOBSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (491, 25, N'NOCAIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (491, 27, N'NÓVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (494, 15, N'NUEVO COLÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (495, 5, N'NECHÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (495, 17, N'NORCASIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (495, 27, N'NUQUÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (497, 76, N'OBANDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (498, 54, N'OCAÑA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (498, 68, N'OCAMONTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (500, 15, N'OICATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (500, 23, N'MOÑITOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (500, 68, N'OIBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (501, 5, N'OLAYA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (502, 68, N'ONZAGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (503, 41, N'OPORAPA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (504, 73, N'ORTEGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (506, 25, N'VENECIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (506, 52, N'OSPINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (507, 15, N'OTANCHE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (508, 70, N'OVEJAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (511, 15, N'PACHAVITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (511, 97, N'PACOA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (513, 17, N'PÁCORA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (513, 19, N'PADILLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (513, 25, N'PACHO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (514, 15, N'PÁEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (516, 15, N'PAIPA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (517, 19, N'PAEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (517, 20, N'PAILITAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (518, 15, N'PAJARITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (518, 25, N'PAIME')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (518, 41, N'PAICOL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (518, 54, N'PAMPLONA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (520, 8, N'PALMAR DE VARELA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (520, 52, N'FRANCISCO PIZARRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (520, 54, N'PAMPLONITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (520, 73, N'PALOCABILDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (520, 76, N'PALMIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (522, 15, N'PANQUEBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (522, 68, N'PALMAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (523, 70, N'PALMITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (524, 17, N'PALESTINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (524, 25, N'PANDI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (524, 41, N'PALERMO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (524, 68, N'PALMAS DEL SOCORRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (524, 99, N'LA PRIMAVERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (530, 25, N'PARATEBUENO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (530, 41, N'PALESTINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (530, 91, N'PUERTO ALEGRÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (531, 15, N'PAUNA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (532, 19, N'PATÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (533, 15, N'PAYA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (533, 19, N'PIAMONTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (533, 68, N'PÁRAMO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (535, 25, N'PASCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (536, 91, N'PUERTO ARICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (537, 15, N'PAZ DE RÍO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (540, 52, N'POLICARPA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (540, 91, N'PUERTO NARIÑO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (541, 5, N'PEÑOL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (541, 17, N'PENSILVANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (541, 47, N'PEDRAZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (542, 15, N'PESCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (543, 5, N'PEQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (545, 47, N'PIJIÑO DEL CARMEN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (547, 68, N'PIEDECUESTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (547, 73, N'PIEDRAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (548, 19, N'PIENDAMÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (548, 41, N'PITAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (548, 63, N'PIJAO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (549, 8, N'PIOJÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (549, 13, N'PINILLOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (549, 68, N'PINCHOTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (550, 15, N'PISBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (550, 20, N'PELAYA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (551, 41, N'PITALITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (551, 47, N'PIVIJAY')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (553, 54, N'PUERTO SANTANDER')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (555, 23, N'PLANETA RICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (555, 47, N'PLATO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (555, 73, N'PLANADAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (558, 8, N'POLONUEVO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (560, 8, N'PONEDERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (560, 44, N'MANAURE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (560, 52, N'POTOSÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (563, 73, N'PRADO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (563, 76, N'PRADERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (564, 88, N'PROVIDENCIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (565, 52, N'PROVIDENCIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (568, 50, N'PUERTO GAITÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (568, 86, N'PUERTO ASÍS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (569, 86, N'PUERTO CAICEDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (570, 20, N'PUEBLO BELLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (570, 23, N'PUEBLO NUEVO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (570, 47, N'PUEBLOVIEJO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (571, 86, N'PUERTO GUZMÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (572, 15, N'PUERTO BOYACÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (572, 25, N'PUERTO SALGAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (572, 66, N'PUEBLO RICO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (572, 68, N'PUENTE NACIONAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (573, 8, N'PUERTO COLOMBIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (573, 19, N'PUERTO TEJADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (573, 50, N'PUERTO LÓPEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (573, 52, N'PUERRES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (573, 68, N'PUERTO PARRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (573, 86, N'LEGUÍZAMO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (574, 23, N'PUERTO ESCONDIDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (575, 68, N'PUERTO WILCHES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (576, 5, N'PUEBLORRICO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (577, 50, N'PUERTO LLERAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (579, 5, N'PUERTO BERRÍO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (580, 13, N'REGIDOR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (580, 15, N'QUÍPAMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (580, 23, N'PUERTO LIBERTADOR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (580, 25, N'PULÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (580, 27, N'RÍO IRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (585, 5, N'PUERTO NARE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (585, 19, N'PURACÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (585, 52, N'PUPIALES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (585, 73, N'PURIFICACIÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (586, 23, N'PURÍSIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (590, 50, N'PUERTO RICO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (591, 5, N'PUERTO TRIUNFO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (591, 81, N'PUERTO RONDÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (592, 18, N'PUERTO RICO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (592, 25, N'QUEBRADANEGRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (594, 25, N'QUETAME')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (594, 63, N'QUIMBAYA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (594, 66, N'QUINCHÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (596, 25, N'QUIPILE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (599, 15, N'RAMIRIQUÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (599, 25, N'APULO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (599, 54, N'RAGONVALIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (600, 13, N'RÍO VIEJO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (600, 15, N'RÁQUIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (600, 27, N'RÍO QUITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (604, 5, N'REMEDIOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (605, 47, N'REMOLINO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (606, 8, N'REPELÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (606, 50, N'RESTREPO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (606, 76, N'RESTREPO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (607, 5, N'RETIRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (610, 18, N'SAN JOSÉ DEL FRAGUA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (612, 25, N'RICAURTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (612, 52, N'RICAURTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (614, 17, N'RIOSUCIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (614, 20, N'RÍO DE ORO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (615, 5, N'RIONEGRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (615, 27, N'RIOSUCIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (615, 41, N'RIVERA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (615, 68, N'RIONEGRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (616, 17, N'RISARALDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (616, 73, N'RIOBLANCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (616, 76, N'RIOFRÍO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (620, 13, N'SAN CRISTÓBAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (621, 15, N'RONDÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (621, 20, N'LA PAZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (621, 52, N'ROBERTO PAYÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (622, 19, N'ROSAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (622, 73, N'RONCESVALLES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (622, 76, N'ROLDANILLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (624, 73, N'ROVIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (624, 99, N'SANTA ROSALÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (628, 5, N'SABANALARGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (631, 5, N'SABANETA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (632, 15, N'SABOYÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (634, 8, N'SABANAGRANDE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (638, 8, N'SABANALARGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (638, 15, N'SÁCHICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (642, 5, N'SALGAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (645, 25, N'SAN ANTONIO DEL TEQUENDAMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (646, 15, N'SAMACÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (647, 5, N'SAN ANDRÉS DE CUERQUÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (647, 13, N'SAN ESTANISLAO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (649, 5, N'SAN CARLOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (649, 25, N'SAN BERNARDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (650, 13, N'SAN FERNANDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (650, 44, N'SAN JUAN DEL CESAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (652, 5, N'SAN FRANCISCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (653, 17, N'SALAMINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (653, 25, N'SAN CAYETANO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (654, 13, N'SAN JACINTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (655, 13, N'SAN JACINTO DEL CAUCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (655, 68, N'SABANA DE TORRES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (656, 5, N'SAN JERÓNIMO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (657, 13, N'SAN JUAN NEPOMUCENO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (658, 5, N'SAN JOSÉ DE LA MONTAÑA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (658, 25, N'SAN FRANCISCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (659, 5, N'SAN JUAN DE URABÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (660, 5, N'SAN LUIS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (660, 15, N'SAN EDUARDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (660, 23, N'SAHAGÚN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (660, 27, N'SAN JOSÉ DEL PALMAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (660, 41, N'SALADOBLANCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (660, 47, N'SABANAS DE SAN ANGEL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (660, 54, N'SALAZAR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (662, 17, N'SAMANÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (662, 25, N'SAN JUAN DE RÍO SECO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (663, 94, N'MAPIRIPANA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (664, 5, N'SAN PEDRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (664, 15, N'SAN JOSÉ DE PARE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (665, 5, N'SAN PEDRO DE URABA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (665, 17, N'SAN JOSÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (666, 97, N'TARAIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (667, 5, N'SAN RAFAEL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (667, 13, N'SAN MARTÍN DE LOBA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (667, 15, N'SAN LUIS DE GACENO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (668, 41, N'SAN AGUSTÍN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (669, 68, N'SAN ANDRÉS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (669, 91, N'PUERTO SANTANDER')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (670, 5, N'SAN ROQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (670, 13, N'SAN PABLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (670, 23, N'SAN ANDRÉS SOTAVENTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (670, 54, N'SAN CALIXTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (670, 70, N'SAMPUÉS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (670, 76, N'SAN PEDRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (671, 73, N'SALDAÑA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (672, 23, N'SAN ANTERO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (673, 13, N'SANTA CATALINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (673, 15, N'SAN MATEO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (673, 54, N'SAN CAYETANO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (673, 68, N'SAN BENITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (674, 5, N'SAN VICENTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (675, 8, N'SANTA LUCÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (675, 23, N'SAN BERNARDO DEL VIENTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (675, 47, N'SALAMINA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (675, 73, N'SAN ANTONIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (676, 15, N'SAN MIGUEL DE SEMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (676, 41, N'SANTA MARÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (678, 23, N'SAN CARLOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (678, 52, N'SAMANIEGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (678, 70, N'SAN BENITO ABAD')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (678, 73, N'SAN LUIS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (679, 5, N'SANTA BÁRBARA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (679, 68, N'SAN GIL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (680, 50, N'SAN CARLOS DE GUAROA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (680, 54, N'SANTIAGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (681, 15, N'SAN PABLO DE BORBUR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (682, 66, N'SANTA ROSA DE CABAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (682, 68, N'SAN JOAQUÍN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (683, 13, N'SANTA ROSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (683, 50, N'SAN JUAN DE ARAMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (683, 52, N'SANDONÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (684, 68, N'SAN JOSÉ DE MIRANDA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (685, 8, N'SANTO TOMÁS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (685, 52, N'SAN BERNARDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (686, 5, N'SANTA ROSA DE OSOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (686, 15, N'SANTANA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (686, 23, N'SAN PELAYO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (686, 50, N'SAN JUANITO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (686, 68, N'SAN MIGUEL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (686, 73, N'SANTA ISABEL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (687, 52, N'SAN LORENZO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (687, 66, N'SANTUARIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (688, 13, N'SANTA ROSA DEL SUR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (689, 50, N'SAN MARTÍN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (689, 68, N'SAN VICENTE DE CHUCURÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (690, 5, N'SANTO DOMINGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (690, 15, N'SANTA MARÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (690, 63, N'SALENTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (692, 47, N'SAN SEBASTIÁN DE BUENAVISTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (693, 15, N'SANTA ROSA DE VITERBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (693, 19, N'SAN SEBASTIÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (693, 52, N'SAN PABLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (694, 52, N'SAN PEDRO DE CARTAGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (696, 15, N'SANTA SOFÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (696, 52, N'SANTA BÁRBARA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (697, 5, N'EL SANTUARIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (698, 19, N'SANTANDER DE QUILICHAO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (699, 52, N'SANTACRUZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (701, 19, N'SANTA ROSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (702, 70, N'SAN JUAN DE BETULIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (703, 47, N'SAN ZENÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (705, 68, N'SANTA BÁRBARA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (707, 47, N'SANTA ANA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (708, 70, N'SAN MARCOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (710, 20, N'SAN ALBERTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (711, 50, N'VISTAHERMOSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (713, 70, N'SAN ONOFRE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (717, 70, N'SAN PEDRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (718, 25, N'SASAIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (720, 15, N'SATIVANORTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (720, 47, N'SANTA BÁRBARA DE PINTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (720, 52, N'SAPUYES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (720, 54, N'SARDINATA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (720, 68, N'SANTA HELENA DEL OPÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (723, 15, N'SATIVASUR')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (736, 5, N'SEGOVIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (736, 25, N'SESQUILÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (736, 76, N'SEVILLA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (736, 81, N'SARAVENA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (740, 15, N'SIACHOQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (740, 25, N'SIBATÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (742, 70, N'SAN LUIS DE SINCÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (743, 19, N'SILVIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (743, 25, N'SILVANIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (743, 54, N'SILOS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (744, 13, N'SIMITÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (745, 25, N'SIMIJACA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (745, 27, N'SIPÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (745, 47, N'SITIONUEVO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (745, 68, N'SIMACOTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (749, 86, N'SIBUNDOY')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (750, 20, N'SAN DIEGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (753, 15, N'SOATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (753, 18, N'SAN VICENTE DEL CAGUÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (754, 25, N'SOACHA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (755, 15, N'SOCOTÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (755, 68, N'SOCORRO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (755, 86, N'SAN FRANCISCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (756, 5, N'SONSON')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (756, 18, N'SOLANO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (757, 15, N'SOCHA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (757, 86, N'SAN MIGUEL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (758, 8, N'SOLEDAD')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (758, 25, N'SOPÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (759, 15, N'SOGAMOSO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (760, 13, N'SOPLAVIENTO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (760, 19, N'SOTARA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (760, 86, N'SANTIAGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (761, 5, N'SOPETRÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (761, 15, N'SOMONDOCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (762, 15, N'SORA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (763, 15, N'SOTAQUIRÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (764, 15, N'SORACÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (769, 25, N'SUBACHOQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (770, 8, N'SUAN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (770, 20, N'SAN MARTÍN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (770, 41, N'SUAZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (770, 68, N'SUAITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (770, 73, N'SUÁREZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (771, 70, N'SUCRE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (772, 25, N'SUESCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (773, 68, N'SUCRE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (773, 99, N'CUMARIBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (774, 15, N'SUSACÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (776, 15, N'SUTAMARCHÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (777, 17, N'SUPÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (777, 25, N'SUPATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (777, 97, N'PAPUNAUA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (778, 15, N'SUTATENZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (779, 25, N'SUSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (780, 13, N'TALAIGUA NUEVO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (780, 19, N'SUÁREZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (780, 68, N'SURATÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (781, 25, N'SUTATAUSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (785, 18, N'SOLITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (785, 19, N'SUCRE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (785, 25, N'TABIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (786, 52, N'TAMINANGO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (787, 20, N'TAMALAMEQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (787, 27, N'TADÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (788, 52, N'TANGUA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (789, 5, N'TÁMESIS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (790, 5, N'TARAZÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (790, 15, N'TASCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (791, 41, N'TARQUI')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (792, 5, N'TARSO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (793, 25, N'TAUSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (794, 81, N'TAME')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (797, 25, N'TENA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (797, 41, N'TESALIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (798, 15, N'TENZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (798, 47, N'TENERIFE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (798, 91, N'TARAPACÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (799, 25, N'TENJO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (799, 41, N'TELLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (800, 27, N'UNGUÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (800, 54, N'TEORAMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (801, 41, N'TERUEL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (804, 15, N'TIBANÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (805, 25, N'TIBACUY')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (806, 15, N'TIBASOSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (807, 19, N'TIMBÍO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (807, 23, N'TIERRALTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (807, 25, N'TIBIRITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (807, 41, N'TIMANÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (808, 15, N'TINJACÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (809, 5, N'TITIRIBÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (809, 19, N'TIMBIQUÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (810, 13, N'TIQUISIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (810, 15, N'TIPACOQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (810, 27, N'UNIÓN PANAMERICANA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (810, 54, N'TIBÚ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (814, 15, N'TOCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (815, 25, N'TOCAIMA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (816, 15, N'TOGÜÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (817, 25, N'TOCANCIPÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (819, 5, N'TOLEDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (820, 15, N'TÓPAGA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (820, 54, N'TOLEDO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (820, 68, N'TONA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (820, 70, N'SANTIAGO DE TOLÚ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (821, 19, N'TORIBIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (822, 15, N'TOTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (823, 25, N'TOPAIPÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (823, 70, N'TOLÚ VIEJO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (823, 76, N'TORO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (824, 19, N'TOTORÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (828, 76, N'TRUJILLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (832, 8, N'TUBARÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (832, 15, N'TUNUNGUÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (834, 76, N'TULUÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (835, 15, N'TURMEQUÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (835, 52, N'TUMACO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (836, 13, N'TURBACO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (837, 5, N'TURBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (837, 15, N'TUTA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (838, 13, N'TURBANÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (838, 52, N'TÚQUERRES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (839, 15, N'TUTAZÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (839, 25, N'UBALÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (841, 25, N'UBAQUE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (842, 5, N'URAMITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (842, 15, N'UMBITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (843, 25, N'VILLA DE SAN DIEGO DE UBATE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (845, 19, N'VILLA RICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (845, 25, N'UNE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (845, 76, N'ULLOA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (847, 5, N'URRAO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (847, 44, N'URIBIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (849, 8, N'USIACURÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (851, 25, N'ÚTICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (854, 5, N'VALDIVIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (854, 73, N'VALLE DE SAN JUAN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (855, 23, N'VALENCIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (855, 44, N'URUMITA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (855, 68, N'VALLE DE SAN JOSÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (856, 5, N'VALPARAÍSO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (858, 5, N'VEGACHÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (860, 18, N'VALPARAÍSO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (861, 5, N'VENECIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (861, 15, N'VENTAQUEMADA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (861, 68, N'VÉLEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (861, 73, N'VENADILLO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (862, 25, N'VERGARA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (863, 76, N'VERSALLES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (865, 86, N'VALLE DEL GUAMUEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (867, 17, N'VICTORIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (867, 25, N'VIANÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (867, 68, N'VETAS')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (869, 76, N'VIJES')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (870, 73, N'VILLAHERMOSA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (871, 25, N'VILLAGÓMEZ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (871, 54, N'VILLA CARO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (872, 41, N'VILLAVIEJA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (872, 68, N'VILLANUEVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (873, 5, N'VIGÍA DEL FUERTE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (873, 13, N'VILLANUEVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (873, 17, N'VILLAMARÍA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (873, 25, N'VILLAPINZÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (873, 73, N'VILLARRICA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (874, 44, N'VILLANUEVA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (874, 54, N'VILLA DEL ROSARIO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (875, 25, N'VILLETA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (877, 17, N'VITERBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (878, 25, N'VIOTÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (879, 15, N'VIRACACHÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (883, 94, N'SAN FELIPE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (884, 94, N'PUERTO COLOMBIA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (885, 5, N'YALÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (885, 25, N'YACOPÍ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (885, 41, N'YAGUARÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (885, 52, N'YACUANQUER')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (885, 86, N'VILLAGARZÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (885, 94, N'LA GUADALUPE')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (886, 94, N'CACAHUAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (887, 5, N'YARUMAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (887, 94, N'PANA PANA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (888, 94, N'MORICHAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (889, 97, N'YAVARATÉ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (890, 5, N'YOLOMBÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (890, 76, N'YOTOCO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (892, 76, N'YUMBO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (893, 5, N'YONDÓ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (894, 13, N'ZAMBRANO')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (895, 5, N'ZARAGOZA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (895, 68, N'ZAPATOCA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (895, 76, N'ZARZAL')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (897, 15, N'ZETAQUIRA')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (898, 25, N'ZIPACÓN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (899, 25, N'ZIPAQUIRÁ')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (960, 47, N'ZAPAYÁN')
GO
INSERT [dbo].[Municipio] ([id], [id_departamento], [nombre]) VALUES (980, 47, N'ZONA BANANERA')
GO
SET IDENTITY_INSERT [dbo].[Punto_Venta] ON 

GO
INSERT [dbo].[Punto_Venta] ([id], [id_municipio], [id_departamento], [nombre], [direccion], [telefono], [promedio_calificacion], [latitud], [longitud]) VALUES (1, 1, 17, N'Tienda don Juan', N'Carrera 25 # 23-2', N'8880028', 3.7142857142857144, 5.064236, -75.5124288)
GO
INSERT [dbo].[Punto_Venta] ([id], [id_municipio], [id_departamento], [nombre], [direccion], [telefono], [promedio_calificacion], [latitud], [longitud]) VALUES (2, 2, 5, N'Tienda don Pedro', N'Carrera 50 # 49-1', N'8770077', NULL, 5.791347, -75.4273855)
GO
INSERT [dbo].[Punto_Venta] ([id], [id_municipio], [id_departamento], [nombre], [direccion], [telefono], [promedio_calificacion], [latitud], [longitud]) VALUES (3, 1, 17, N'Tienda don Juan', N'Carrera 25 # 23-2', N'8880028', 0, 5.064236, -75.5124288)
GO
SET IDENTITY_INSERT [dbo].[Punto_Venta] OFF
GO
SET IDENTITY_INSERT [dbo].[Punto_Venta_Insumos] ON 

GO
INSERT [dbo].[Punto_Venta_Insumos] ([id], [id_punto_venta], [id_insumo]) VALUES (1, 1, 1)
GO
INSERT [dbo].[Punto_Venta_Insumos] ([id], [id_punto_venta], [id_insumo]) VALUES (2, 1, 3)
GO
INSERT [dbo].[Punto_Venta_Insumos] ([id], [id_punto_venta], [id_insumo]) VALUES (3, 1, 4)
GO
INSERT [dbo].[Punto_Venta_Insumos] ([id], [id_punto_venta], [id_insumo]) VALUES (4, 2, 1)
GO
INSERT [dbo].[Punto_Venta_Insumos] ([id], [id_punto_venta], [id_insumo]) VALUES (5, 2, 5)
GO
SET IDENTITY_INSERT [dbo].[Punto_Venta_Insumos] OFF
GO
SET IDENTITY_INSERT [dbo].[Reporte_Variacion_Precio] ON 

GO
INSERT [dbo].[Reporte_Variacion_Precio] ([id], [id_punto_venta], [id_insumo], [id_municipio], [id_departamento], [precio], [punto_venta]) VALUES (1, 1, 4, 1, 17, 60000, N'')
GO
INSERT [dbo].[Reporte_Variacion_Precio] ([id], [id_punto_venta], [id_insumo], [id_municipio], [id_departamento], [precio], [punto_venta]) VALUES (2, 2, 4, 2, 5, 65000, N'')
GO
INSERT [dbo].[Reporte_Variacion_Precio] ([id], [id_punto_venta], [id_insumo], [id_municipio], [id_departamento], [precio], [punto_venta]) VALUES (3, 1, 3, 1, 17, 80000, N'')
GO
INSERT [dbo].[Reporte_Variacion_Precio] ([id], [id_punto_venta], [id_insumo], [id_municipio], [id_departamento], [precio], [punto_venta]) VALUES (7, 1, 4, 1, 17, 60000, N'')
GO
SET IDENTITY_INSERT [dbo].[Reporte_Variacion_Precio] OFF
GO
SET IDENTITY_INSERT [dbo].[Tipo_Insumo] ON 

GO
INSERT [dbo].[Tipo_Insumo] ([id], [nombre]) VALUES (1, N'Fertilizante')
GO
INSERT [dbo].[Tipo_Insumo] ([id], [nombre]) VALUES (2, N'Plaguicida')
GO
INSERT [dbo].[Tipo_Insumo] ([id], [nombre]) VALUES (3, N'Medicamento')
GO
INSERT [dbo].[Tipo_Insumo] ([id], [nombre]) VALUES (4, N'Biológico')
GO
INSERT [dbo].[Tipo_Insumo] ([id], [nombre]) VALUES (5, N'Semillas')
GO
INSERT [dbo].[Tipo_Insumo] ([id], [nombre]) VALUES (6, N'Alimentos')
GO
INSERT [dbo].[Tipo_Insumo] ([id], [nombre]) VALUES (7, N'Sales')
GO
SET IDENTITY_INSERT [dbo].[Tipo_Insumo] OFF
GO
SET IDENTITY_INSERT [dbo].[Tipo_Producto] ON 

GO
INSERT [dbo].[Tipo_Producto] ([id], [nombre], [id_tipo_insumo]) VALUES (1, N'15-15-15', 1)
GO
INSERT [dbo].[Tipo_Producto] ([id], [nombre], [id_tipo_insumo]) VALUES (2, N'17-6-18-2', 1)
GO
INSERT [dbo].[Tipo_Producto] ([id], [nombre], [id_tipo_insumo]) VALUES (3, N'10-20-20', 2)
GO
INSERT [dbo].[Tipo_Producto] ([id], [nombre], [id_tipo_insumo]) VALUES (4, N'UREA', 2)
GO
INSERT [dbo].[Tipo_Producto] ([id], [nombre], [id_tipo_insumo]) VALUES (5, N'FOSFATO DIAMONICO (DAP)', 2)
GO
SET IDENTITY_INSERT [dbo].[Tipo_Producto] OFF
GO
ALTER TABLE [dbo].[Calificacion]  WITH CHECK ADD  CONSTRAINT [FK_Calificacion_Punto_Venta] FOREIGN KEY([id_punto_venta])
REFERENCES [dbo].[Punto_Venta] ([id])
GO
ALTER TABLE [dbo].[Calificacion] CHECK CONSTRAINT [FK_Calificacion_Punto_Venta]
GO
ALTER TABLE [dbo].[Insumo]  WITH CHECK ADD  CONSTRAINT [FK_Insumo_Tipo_Producto] FOREIGN KEY([id_tipo_producto])
REFERENCES [dbo].[Tipo_Producto] ([id])
GO
ALTER TABLE [dbo].[Insumo] CHECK CONSTRAINT [FK_Insumo_Tipo_Producto]
GO
ALTER TABLE [dbo].[Insumo_Departamento_Precio]  WITH CHECK ADD  CONSTRAINT [FK_Insumo_Departamento_Precio_Departamento] FOREIGN KEY([id_departamento])
REFERENCES [dbo].[Departamento] ([id])
GO
ALTER TABLE [dbo].[Insumo_Departamento_Precio] CHECK CONSTRAINT [FK_Insumo_Departamento_Precio_Departamento]
GO
ALTER TABLE [dbo].[Insumo_Departamento_Precio]  WITH CHECK ADD  CONSTRAINT [FK_Insumo_Departamento_Precio_Insumo] FOREIGN KEY([id_insumo])
REFERENCES [dbo].[Insumo] ([id])
GO
ALTER TABLE [dbo].[Insumo_Departamento_Precio] CHECK CONSTRAINT [FK_Insumo_Departamento_Precio_Insumo]
GO
ALTER TABLE [dbo].[Insumo_Municipio_Precio]  WITH CHECK ADD  CONSTRAINT [FK_Insumo_Municipio_Precio_Insumo] FOREIGN KEY([id_insumo])
REFERENCES [dbo].[Insumo] ([id])
GO
ALTER TABLE [dbo].[Insumo_Municipio_Precio] CHECK CONSTRAINT [FK_Insumo_Municipio_Precio_Insumo]
GO
ALTER TABLE [dbo].[Insumo_Municipio_Precio]  WITH CHECK ADD  CONSTRAINT [FK_Insumo_Municipio_Precio_Municipio] FOREIGN KEY([id_municipio], [id_departaimento])
REFERENCES [dbo].[Municipio] ([id], [id_departamento])
GO
ALTER TABLE [dbo].[Insumo_Municipio_Precio] CHECK CONSTRAINT [FK_Insumo_Municipio_Precio_Municipio]
GO
ALTER TABLE [dbo].[Municipio]  WITH CHECK ADD  CONSTRAINT [FK_Municipio_Departamento] FOREIGN KEY([id_departamento])
REFERENCES [dbo].[Departamento] ([id])
GO
ALTER TABLE [dbo].[Municipio] CHECK CONSTRAINT [FK_Municipio_Departamento]
GO
ALTER TABLE [dbo].[Punto_Venta]  WITH CHECK ADD  CONSTRAINT [FK_Punto_Venta_Municipio] FOREIGN KEY([id_municipio], [id_departamento])
REFERENCES [dbo].[Municipio] ([id], [id_departamento])
GO
ALTER TABLE [dbo].[Punto_Venta] CHECK CONSTRAINT [FK_Punto_Venta_Municipio]
GO
ALTER TABLE [dbo].[Punto_Venta_Insumos]  WITH CHECK ADD  CONSTRAINT [FK_Punto_Venta_Insumos_Insumo] FOREIGN KEY([id_insumo])
REFERENCES [dbo].[Insumo] ([id])
GO
ALTER TABLE [dbo].[Punto_Venta_Insumos] CHECK CONSTRAINT [FK_Punto_Venta_Insumos_Insumo]
GO
ALTER TABLE [dbo].[Punto_Venta_Insumos]  WITH CHECK ADD  CONSTRAINT [FK_Punto_Venta_Insumos_Punto_Venta] FOREIGN KEY([id_punto_venta])
REFERENCES [dbo].[Punto_Venta] ([id])
GO
ALTER TABLE [dbo].[Punto_Venta_Insumos] CHECK CONSTRAINT [FK_Punto_Venta_Insumos_Punto_Venta]
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio]  WITH CHECK ADD  CONSTRAINT [FK_Reporte_Variacion_Precio_Insumo] FOREIGN KEY([id_insumo])
REFERENCES [dbo].[Insumo] ([id])
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio] CHECK CONSTRAINT [FK_Reporte_Variacion_Precio_Insumo]
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio]  WITH CHECK ADD  CONSTRAINT [FK_Reporte_Variacion_Precio_Municipio] FOREIGN KEY([id_municipio], [id_departamento])
REFERENCES [dbo].[Municipio] ([id], [id_departamento])
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio] CHECK CONSTRAINT [FK_Reporte_Variacion_Precio_Municipio]
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio]  WITH CHECK ADD  CONSTRAINT [FK_Reporte_Variacion_Precio_Punto_Venta] FOREIGN KEY([id_punto_venta])
REFERENCES [dbo].[Punto_Venta] ([id])
GO
ALTER TABLE [dbo].[Reporte_Variacion_Precio] CHECK CONSTRAINT [FK_Reporte_Variacion_Precio_Punto_Venta]
GO
ALTER TABLE [dbo].[Tipo_Producto]  WITH CHECK ADD  CONSTRAINT [FK_Tipo_Producto_Tipo_Insumo] FOREIGN KEY([id_tipo_insumo])
REFERENCES [dbo].[Tipo_Insumo] ([id])
GO
ALTER TABLE [dbo].[Tipo_Producto] CHECK CONSTRAINT [FK_Tipo_Producto_Tipo_Insumo]
GO
