﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BusinessLayer;
using DataContracts;

namespace ServiceLayer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class ServicioInsumos : IServicioInsumos
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public List<DepartamentoDTO> ObtenerDepartamentos()
        {
            var response = BusinessContext.ObtenerDepartamentos();

            return response;
        }

        public List<MunicipioDTO> ObtenerMunicipiosPorDepartamento(int idDepartamento)
        {
            var response = BusinessContext.ObtenerMunicipiosPorDepartamento(idDepartamento);

            return response;
        }

        public List<TipoInsumoDTO> ObtenerTiposDeInsumo()
        {
            var response = BusinessContext.ObtenerTiposDeInsumo();

            return response;
        }

        public List<TipoProductoDTO> ObtenerTiposDeProductoPorTipoInsumo(int idTipoInsumo)
        {
            var response = BusinessContext.ObtenerTiposDeProductoPorTipoInsumo(idTipoInsumo);

            return response;
        }

        public List<InsumoDTO> ObtenerInsumosPorMunicipioTipoProducto(int idMunicipio, int idDepartamento, string idTipoProducto)
        {
            var response = BusinessContext.ObtenerInsumos(idMunicipio, idDepartamento, idTipoProducto);

            return response;
        }

        public List<InsumoDTO> ObtenerInsumos()
        {
            var response = BusinessContext.ObtenerInsumos();

            return response;
        }

        public string ObtenerPromedioNacional(int idTipoProducto)
        {
            var response = BusinessContext.ObtenerPromedioNacional(idTipoProducto);

            return response;
        }

        public string ObtenerPromedioDepartamental(int idInsumo, int idDepartamento)
        {
            var response = BusinessContext.ObtenerPromedioDepartamental(idInsumo, idDepartamento);

            return response;
        }

        public string ObtenerPromedioMunicipal(int idInsumo, int idMunicipio, int idDepartamento)
        {
            var response = BusinessContext.ObtenerPromedioMunicipal(idInsumo, idMunicipio, idDepartamento);

            return response;
        }

        public bool ReportarVariacionDePrecio(int precioProducto, string idPuntoVenta, string nombreTienda, int idInsumo, int idMunicipio, int idDepartamento)
        {
            var response = BusinessContext.ReportarVariacionDePrecio(precioProducto, idPuntoVenta, nombreTienda, idInsumo, idMunicipio, idDepartamento);

            return response;
        }

        public List<TiendaDTO> ObtenerPuntosDeVenta(int idMunicipio, int idDepartamento)
        {
            var response = BusinessContext.ObtenerPuntosDeVenta(idMunicipio, idDepartamento);

            return response;
        }

        public bool GuardarCalificacionPorPuntoDeVenta(int idTienda, int calificacionUsuario)
        {
            var response = BusinessContext.GuardarCalificacionPorPuntoDeVenta(idTienda, calificacionUsuario);

            return response;
        }

        public List<ConsejoDTO> ObtenerConsejos()
        {
            var response = BusinessContext.ObtenerConsejos();

            return response;
        }

        public List<BoletinDTO> ObtenerBoletines()
        {
            var response = BusinessContext.ObtenerBoletinesInformativos();

            return response;
        }

        public bool GuardarPuntoDeVenta(string nombre, string direccion, string promedio, string telefono, int idMunicipio, int idDepartamento, string latitud, string longitud)
        {
            var response = BusinessContext.GuardarPuntoDeVenta(nombre, direccion, promedio, telefono, idMunicipio, idDepartamento, latitud, longitud);

            return response;
        }
    }
}
