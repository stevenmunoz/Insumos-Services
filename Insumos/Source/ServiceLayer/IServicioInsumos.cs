﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DataContracts;

namespace ServiceLayer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IServicioInsumos
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        List<DepartamentoDTO> ObtenerDepartamentos();

        [OperationContract]
        List<MunicipioDTO> ObtenerMunicipiosPorDepartamento(int idDepartamento);

        [OperationContract]
        List<TipoInsumoDTO> ObtenerTiposDeInsumo();

        [OperationContract]
        List<TipoProductoDTO> ObtenerTiposDeProductoPorTipoInsumo(int idTipoInsumo);

        [OperationContract]
        List<InsumoDTO> ObtenerInsumosPorMunicipioTipoProducto(int idMunicipio, int idDepartamento, string idTipoProducto);

        [OperationContract]
        List<InsumoDTO> ObtenerInsumos();

        [OperationContract] 
        string ObtenerPromedioNacional(int idTipoProducto);

        [OperationContract]
        string ObtenerPromedioDepartamental(int idInsumo, int idDepartamento);

        [OperationContract]
        string ObtenerPromedioMunicipal(int idInsumo, int idMunicipio, int idDepartamento);

        [OperationContract]
        bool ReportarVariacionDePrecio(int precioProducto, string idPuntoVenta, string nombreTienda, int idInsumo, int idMunicipio, int idDepartamento);

        [OperationContract]
        List<TiendaDTO> ObtenerPuntosDeVenta(int idMunicipio, int idDepartamento);

        [OperationContract]
        bool GuardarCalificacionPorPuntoDeVenta(int idTienda, int calificacionUsuario);

        [OperationContract]
        List<ConsejoDTO> ObtenerConsejos();

        [OperationContract]
        List<BoletinDTO> ObtenerBoletines();

        [OperationContract]
        bool GuardarPuntoDeVenta(string nombre, string direccion, string promedio, string telefono, int idMunicipio, int idDepartamento, string latitud, string longitud);

        // TODO: Add your service operations here
    }
}
