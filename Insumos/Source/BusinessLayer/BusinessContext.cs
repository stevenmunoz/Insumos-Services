﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using DataContracts;
using System.Collections;
using System.Globalization;

namespace BusinessLayer
{
    public static class BusinessContext
    {
        static DataContext context;

        static BusinessContext()
        {
            if (context == null)
            {
                context = new DataContext();
                context.Connection.Open();
            }
        }

        // obtener departamentos 1a ok
        public static List<DepartamentoDTO> ObtenerDepartamentos()
        {
            List<DepartamentoDTO> list = new List<DepartamentoDTO>();

            var result = context.Departamento.ToList();

            if (result.Count() > 0)
            {
                foreach (var departamento in result)
                {
                    DepartamentoDTO dto = new DepartamentoDTO
                    {
                        IdDepartamento = departamento.id,
                        NombreDepartamento = departamento.nombre
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        // obtener municipios 1b ok
        public static List<MunicipioDTO> ObtenerMunicipiosPorDepartamento(int idDepartamento)
        {
            List<MunicipioDTO> list = new List<MunicipioDTO>();

            var result = context.Municipio.Where(m => m.id_departamento == idDepartamento).ToList();

            if (result.Count() > 0)
            {
                foreach (var municipio in result)
                {
                    MunicipioDTO dto = new MunicipioDTO
                    {
                        IdMunicipio = municipio.id,
                        IdDepartamento = municipio.id_departamento,
                        NombreMunicipio = municipio.nombre
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        // obtener tipos de insumo 1c ok
        public static List<TipoInsumoDTO> ObtenerTiposDeInsumo()
        {
            List<TipoInsumoDTO> list = new List<TipoInsumoDTO>();

            var result = context.Tipo_Insumo.ToList().OrderBy(t => t.nombre);

            if (result.Count() > 0)
            {
                foreach (var tipoInsumo in result)
                {
                    TipoInsumoDTO dto = new TipoInsumoDTO
                    {
                        IdTipoInsumo = tipoInsumo.id,
                        NombreTipoInsumo = tipoInsumo.nombre
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        // obtener tipos de producto por tipo de insumo 1d ok
        public static List<TipoProductoDTO> ObtenerTiposDeProductoPorTipoInsumo(int idTipoInsumo)
        {
            List<TipoProductoDTO> list = new List<TipoProductoDTO>();

            var result = (idTipoInsumo == 0) ? context.Tipo_Producto :
                context.Tipo_Producto.Where(tp => tp.id_tipo_insumo == idTipoInsumo);

            if (result.Count() > 0)
            {
                foreach (var tipoProducto in result)
                {
                    TipoProductoDTO dto = new TipoProductoDTO
                    {
                        IdTipoProducto = tipoProducto.id,
                        NombreTipoProducto = tipoProducto.nombre
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        // obtener insumos 1e ok 
        public static List<InsumoDTO> ObtenerInsumos(int idMunicipio, int idDepartamento, string idTipoProducto)
        {
            List<InsumoDTO> list = new List<InsumoDTO>();

            if (idTipoProducto == null || idTipoProducto == string.Empty)
            {
                var result = (from insumos in context.Insumo
                              join puntoinsumos in context.Punto_Venta_Insumos on insumos.id equals puntoinsumos.id_insumo
                              join puntos in context.Punto_Venta on puntoinsumos.id_punto_venta equals puntos.id
                              where puntos.id_municipio == idMunicipio && puntos.id_departamento == idDepartamento
                              select insumos).ToList();

                if (result.Count() > 0)
                {
                    foreach (var insumo in result)
                    {
                        InsumoDTO dto = new InsumoDTO
                        {
                            IdInsumo = insumo.id,
                            TipoProducto = context.Tipo_Producto.FirstOrDefault(p => p.id == insumo.id_tipo_producto).nombre,
                            TipoProduccion = insumo.tipo_produccion,
                            CantidadPresentacion = insumo.cantidad_presentacion.ToString(),
                            UnidadPresentacion = insumo.unidad_presentacion,
                            PrecioMinisterio = insumo.precio_ministerio.ToString(),
                            PrecioPromedioReportado = insumo.precio_promedio_reportado.ToString()
                        };

                        list.Add(dto);
                    }
                }
            }
            else
            {
                int idtp = Convert.ToInt32(idTipoProducto);

                var result = (from insumos in context.Insumo
                              join puntoinsumos in context.Punto_Venta_Insumos on insumos.id equals puntoinsumos.id_insumo
                              join puntos in context.Punto_Venta on puntoinsumos.id_punto_venta equals puntos.id
                              join tipoproductos in context.Tipo_Producto on insumos.id_tipo_producto equals tipoproductos.id
                              where tipoproductos.id == idtp && puntos.id_municipio == idMunicipio
                              && puntos.id_departamento == idDepartamento
                              select insumos).ToList();

                if (result.Count() > 0)
                {
                    foreach (var insumo in result)
                    {
                        InsumoDTO dto = new InsumoDTO
                        {
                            IdInsumo = insumo.id,
                            TipoProducto = context.Tipo_Producto.FirstOrDefault(p => p.id == insumo.id_tipo_producto).nombre,
                            TipoProduccion = insumo.tipo_produccion,
                            CantidadPresentacion = insumo.cantidad_presentacion.ToString(),
                            UnidadPresentacion = insumo.unidad_presentacion,
                            PrecioMinisterio = insumo.precio_ministerio.ToString(),
                            PrecioPromedioReportado = insumo.precio_promedio_reportado.ToString()
                        };

                        list.Add(dto);
                    }
                }
            }

            return list;
        }

        // obtener insumos 1f ok
        public static List<InsumoDTO> ObtenerInsumos()
        {
            List<InsumoDTO> list = new List<InsumoDTO>();

            var result = context.Insumo.ToList();

            if (result.Count() > 0)
            {
                foreach (var insumo in result)
                {
                    InsumoDTO dto = new InsumoDTO
                    {
                        IdInsumo = insumo.id,
                        TipoProducto = context.Tipo_Producto.FirstOrDefault(p => p.id == insumo.id_tipo_producto).nombre,
                        TipoProduccion = insumo.tipo_produccion,
                        CantidadPresentacion = insumo.cantidad_presentacion.ToString(),
                        UnidadPresentacion = insumo.unidad_presentacion,
                        PrecioMinisterio = insumo.precio_ministerio.ToString(),
                        PrecioPromedioReportado = insumo.precio_promedio_reportado.ToString()
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        // obtener promedio nacional por grupo o nombre producto 2a ok
        public static string ObtenerPromedioNacional(int idTipoProducto)
        {
            string result = string.Empty;

            var insumos = context.Insumo.Where(i => i.id_tipo_producto == idTipoProducto);

            if (insumos.Count() > 0)
            {
                var promedio = insumos.Average(i => i.precio_ministerio);

                result = promedio.ToString();
            }
            else
            {
                result = "no se encontraron datos";
            }

            return result;
        }

        // obtener promedio departamental por grupo o nombre producto 2b ok
        public static string ObtenerPromedioDepartamental(int idInsumo, int idDepartamento)
        {
            string result = string.Empty;

            var idps = (from idp in context.Insumo_Departamento_Precio
                        join insumos in context.Insumo on idp.id_insumo equals insumos.id
                        join dptos in context.Departamento on idp.id_departamento equals dptos.id
                        where dptos.id == idDepartamento && idp.id_insumo == idInsumo
                        select idp).ToList();

            if (idps.Count() > 0)
            {
                var promedio = idps.Average(i => i.precio_departamento);

                result = promedio.ToString();
            }
            else
            {
                result = "no se encontraron datos";
            }

            return result;
        }

        // obtener promedio municipal por grupo o nombre producto 2c ok
        public static string ObtenerPromedioMunicipal(int idInsumo, int idMunicipio, int idDepartamento)
        {
            string result = string.Empty;

            var imps = (from imp in context.Insumo_Municipio_Precio
                        join insumos in context.Insumo on imp.id_insumo equals insumos.id
                        join mpios in context.Municipio on imp.id_municipio equals mpios.id
                        where mpios.id == idMunicipio && imp.id_insumo == idInsumo
                        && imp.id_departaimento == idDepartamento
                        select imp).ToList();

            if (imps.Count() > 0)
            {
                var promedio = imps.Average(i => i.precio_municipal);

                result = promedio.ToString();
            }
            else
            {
                result = "no se encontraron datos";
            }

            return result;
        }

        // reportar variacion de precio 3
        public static bool ReportarVariacionDePrecio(int precioProducto, string idPuntoVenta, string nombreTienda, int idInsumo, int idMunicipio, int idDepartamento)
        {
            bool result = true;

            try
            {
                Reporte_Variacion_Precio reporte = new Reporte_Variacion_Precio
                {
                    precio = precioProducto,
                    id_insumo = idInsumo,
                    punto_venta = nombreTienda,
                    id_municipio = idMunicipio,
                    id_departamento = idDepartamento
                };

                if (idPuntoVenta == null || idPuntoVenta == string.Empty)
                {
                    reporte.id_punto_venta = null;
                }
                else
                {
                    reporte.id_punto_venta = Convert.ToInt32(idPuntoVenta);
                }

                context.AddToReporte_Variacion_Precio(reporte);
                context.SaveChanges();

                var insumo = context.Insumo.First(i => i.id == idInsumo);

                int? idPv = Convert.ToInt32(idPuntoVenta);

                var reportes = context.Reporte_Variacion_Precio.Where(r => r.id_insumo == idInsumo &&
                               r.id_punto_venta == idPv && r.id_municipio == idMunicipio &&
                               r.id_departamento == idDepartamento).ToList();

                if (reportes.Count() > 0)
                {
                    insumo.precio_promedio_reportado = reportes.Average(r => r.precio);
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                // capturar excepcion y registrarla: pendiente
                result = false;
            }


            return result;
        }

        //obtener puntos de venta 4a
        public static List<TiendaDTO> ObtenerPuntosDeVenta(int idMunicipio, int idDepartamento)
        {
            List<TiendaDTO> list = new List<TiendaDTO>();

            var result = context.Punto_Venta.Where(p => p.id_municipio == idMunicipio && p.id_departamento == idDepartamento);

            if (result.Count() > 0)
            {
                foreach (var tienda in result)
                {
                    TiendaDTO dto = new TiendaDTO
                    {
                        IdPuntoVenta = tienda.id,
                        NombreTienda = tienda.nombre,
                        DireccionTienda = tienda.direccion,
                        PromedioCalificacion = tienda.promedio_calificacion,
                        Telefono = tienda.telefono,
                        IdDepartamento = tienda.id_departamento,
                        IdMunicipio = tienda.id_municipio,
                        Latitud = tienda.latitud,
                        Longitud = tienda.longitud
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        // guardar calificacion para una tienda 4b
        public static bool GuardarCalificacionPorPuntoDeVenta(int idTienda, int calificacionUsuario)
        {
            bool result = true;

            try
            {
                Calificacion calificacion = new Calificacion
                {
                    calificacion1 = calificacionUsuario,
                    id_punto_venta = idTienda
                };

                context.AddToCalificacion(calificacion);
                context.SaveChanges();

                var calificaciones = context.Calificacion.Where(c => c.id_punto_venta == idTienda);

                double promedio = 0;

                if (calificaciones.Count() > 0)
                {
                    promedio = calificaciones.Average(c => c.calificacion1);
                }

                var tienda = context.Punto_Venta.First(t => t.id == idTienda);

                tienda.promedio_calificacion = promedio;

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                // capturar excepcion y registrarla: pendiente
                result = false;
            }

            return result;
        }

        // obtener consejos 5
        public static List<ConsejoDTO> ObtenerConsejos()
        {
            List<ConsejoDTO> list = new List<ConsejoDTO>();

            var result = context.Consejo.ToList();

            if (result.Count() > 0)
            {
                foreach (var consejo in result)
                {
                    ConsejoDTO dto = new ConsejoDTO
                    {
                        TituloConsejo = consejo.titulo,
                        DescripcionConsejo = consejo.descripcion
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        // obtener boletines 6
        public static List<BoletinDTO> ObtenerBoletinesInformativos()
        {
            List<BoletinDTO> list = new List<BoletinDTO>();

            var result = context.Boletin.ToList();

            if (result.Count() > 0)
            {
                foreach (var boletin in result)
                {
                    BoletinDTO dto = new BoletinDTO
                    {
                        TituloBoletin = boletin.titulo,
                        UrlImagen = boletin.imagen,
                        UrlEnlace = boletin.url
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        // almacenar punto de venta 7
        public static bool GuardarPuntoDeVenta(string nombre, string direccion, string promedio, string telefono, int idMunicipio, int idDepartamento, string latitud, string longitud)
        {
            bool result = true;

            try
            {
                Punto_Venta tienda = new Punto_Venta
                {
                    nombre = nombre,
                    direccion = direccion,
                    telefono = telefono,
                    id_municipio = idMunicipio,
                    id_departamento = idDepartamento
                };

                if (promedio == null || promedio == string.Empty)
                {
                    tienda.promedio_calificacion = null;
                }
                else
                {
                    tienda.promedio_calificacion = Convert.ToDouble(promedio);
                }

                if ((latitud == null && longitud == null) || (latitud == string.Empty && longitud == string.Empty))
                {
                    tienda.latitud = null;
                    tienda.longitud = null;
                }
                else
                {
                    string lat = latitud.Replace(',', '.');
                    string lon = longitud.Replace(',', '.');

                    double dlat;
                    double dlon;

                    double.TryParse(lat, NumberStyles.Any, CultureInfo.InvariantCulture, out dlat);
                    double.TryParse(lon, NumberStyles.Any, CultureInfo.InvariantCulture, out dlon);

                    tienda.latitud = dlat;
                    tienda.longitud = dlon;
                }

                context.AddToPunto_Venta(tienda);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                // capturar excepcion y registrarla: pendiente
                result = false;
            }

            return result;
        }
    }
}
