﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DataContracts
{
    [DataContract]
    public class TiendaDTO
    {
        int idPuntoVenta;
        string nombreTienda;
        string direccionTienda;
        double? promedioCalificacion;
        string telefono;
        int idMunicipio;
        int idDepartamento;
        double? latitud;
        double? longitud;

        [DataMember]
        public int IdPuntoVenta
        {
            get { return idPuntoVenta; }
            set { idPuntoVenta = value; }
        }

        [DataMember]
        public string NombreTienda
        {
            get { return nombreTienda; }
            set { nombreTienda = value; }
        }

        [DataMember]
        public string DireccionTienda
        {
            get { return direccionTienda; }
            set { direccionTienda = value; }
        }

        [DataMember]
        public double? PromedioCalificacion
        {
            get { return promedioCalificacion; }
            set { promedioCalificacion = value; }
        }

        [DataMember]
        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        [DataMember]
        public int IdMunicipio
        {
            get { return idMunicipio; }
            set { idMunicipio = value; }
        }

        [DataMember]
        public int IdDepartamento
        {
            get { return idDepartamento; }
            set { idDepartamento = value; }
        }

        [DataMember]
        public double? Latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }

        [DataMember]
        public double? Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }
    }
}
