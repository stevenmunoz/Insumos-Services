﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DataContracts
{
    [DataContract]
    public class BoletinDTO
    {
        string tituloBoletin;
        string urlEnlace;
        string urlImagen;

        [DataMember]
        public string TituloBoletin
        {
            get { return tituloBoletin; }
            set { tituloBoletin = value; }
        }

        [DataMember]
        public string UrlEnlace
        {
            get { return urlEnlace; }
            set { urlEnlace = value; }
        }

        [DataMember]
        public string UrlImagen
        {
            get { return urlImagen; }
            set { urlImagen = value; }
        }
    }
}
