﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DataContracts
{
    [DataContract]
    public class ConsejoDTO
    {
        string tituloConsejo;
        string descripcionConsejo;

        [DataMember]
        public string TituloConsejo
        {
            get { return tituloConsejo; }
            set { tituloConsejo = value; }
        }

        [DataMember]
        public string DescripcionConsejo
        {
            get { return descripcionConsejo; }
            set { descripcionConsejo = value; }
        }
    }
}
