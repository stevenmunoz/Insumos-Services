﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DataContracts
{
    [DataContract]
    public class MunicipioDTO
    {
        int idDepartamento;
        int idMunicipio;
        string nombreMunicipio;

        [DataMember]
        public int IdDepartamento
        {
            get { return idDepartamento; }
            set { idDepartamento = value; }
        }

        [DataMember]
        public int IdMunicipio
        {
            get { return idMunicipio; }
            set { idMunicipio = value; }
        }

        [DataMember]
        public string NombreMunicipio
        {
            get { return nombreMunicipio; }
            set { nombreMunicipio = value; }
        }
    }
}
