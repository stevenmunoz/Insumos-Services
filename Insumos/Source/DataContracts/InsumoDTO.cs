﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DataContracts
{
    [DataContract]
    public class InsumoDTO
    {
        int idInsumo;
        string tipoProducto;
        string tipoProduccion;
        string cantidadPresentacion;
        string unidadPresentacion;
        string precioMinisterio;
        string precioPromedioReportado;

        [DataMember]
        public int IdInsumo
        {
            get { return idInsumo; }
            set { idInsumo = value; }
        }

        [DataMember]
        public string TipoProducto
        {
            get { return tipoProducto; }
            set { tipoProducto = value; }
        }

        [DataMember]
        public string TipoProduccion
        {
            get { return tipoProduccion; }
            set { tipoProduccion = value; }
        }

        [DataMember]
        public string CantidadPresentacion
        {
            get { return cantidadPresentacion; }
            set { cantidadPresentacion = value; }
        }

        [DataMember]
        public string UnidadPresentacion
        {
            get { return unidadPresentacion; }
            set { unidadPresentacion = value; }
        }

        [DataMember]
        public string PrecioMinisterio
        {
            get { return precioMinisterio; }
            set { precioMinisterio = value; }
        }

        [DataMember]
        public string PrecioPromedioReportado
        {
            get { return precioPromedioReportado; }
            set { precioPromedioReportado = value; }
        }
    }
}
