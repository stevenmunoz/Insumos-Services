﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DataContracts
{
    [DataContract]
    public class DepartamentoDTO
    {
        int idDepartamento;
        string nombreDepartamento;

        [DataMember]
        public int IdDepartamento
        {
            get { return idDepartamento; }
            set { idDepartamento = value; }
        }

        [DataMember]
        public string NombreDepartamento
        {
            get { return nombreDepartamento; }
            set { nombreDepartamento = value; }
        }
    }
}
