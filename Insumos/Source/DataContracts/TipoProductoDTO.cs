﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DataContracts
{
    [DataContract]
    public class TipoProductoDTO
    {
        int idTipoProducto;
        string nombreTipoProducto;

        [DataMember]
        public int IdTipoProducto
        {
            get { return idTipoProducto; }
            set { idTipoProducto = value; }
        }

        [DataMember]
        public string NombreTipoProducto
        {
            get { return nombreTipoProducto; }
            set { nombreTipoProducto = value; }
        }
    }
}
