﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DataContracts
{
    [DataContract]
    public class TipoInsumoDTO
    {
        int idTipoInsumo;
        string nombreTipoInsumo;

        [DataMember]
        public int IdTipoInsumo
        {
            get { return idTipoInsumo; }
            set { idTipoInsumo = value; }
        }

        [DataMember]
        public string NombreTipoInsumo
        {
            get { return nombreTipoInsumo; }
            set { nombreTipoInsumo = value; }
        }
    }
}
