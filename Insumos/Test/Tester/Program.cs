﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tester.Insumos;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicioInsumosClient client = new ServicioInsumosClient();

            try
            {
                //string val = client.GetData(4);

                
                Console.WriteLine("Prueba Insumos\n");

                var departamentos = client.ObtenerDepartamentos();

                Console.WriteLine("Departamentos (" + departamentos.Count() + ")");

                foreach (var d in departamentos)
                {
                    Console.WriteLine(d.NombreDepartamento);
                }

                var municipios = client.ObtenerMunicipiosPorIdDepartamento(5);

                Console.WriteLine("Municipios (Dpto 5:" + municipios.Count() + ")");

                foreach (var m in municipios)
                {
                    Console.WriteLine(m.NombreMunicipio);
                }
                
                /*
                var result1 = client.ObtenerDepartamentos();

                Console.WriteLine("\n\n" + result1.Count());

                var result2 = client.ObtenerMunicipiosPorIdDepartamento(8);

                Console.WriteLine("\n\n" + result2.Count());

                var result3 = client.ObtenerTiposDeProductoPorTipoInsumo(1);

                Console.WriteLine("\n\n" + result3.Count());

                var result4 = client.ObtenerTiposDeInsumo();

                Console.WriteLine("\n\n" + result4.Count());

                var result5 = client.ObtenerInsumosAgropecuarios(5, 5002, 1, 112);

                Console.WriteLine("\n\n" + result5.Count());

                var result6 = client.ObtenerPromedioNacional(1, 112);

                Console.WriteLine("\n\n" + result6);

                var result7 = client.ObtenerBoletines();

                Console.WriteLine("\n\n" + result7.Count());

                var result8 = client.ObtenerConsejos();

                Console.WriteLine("\n\n" + result8.Count());
                */
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                client.Abort();

                Console.WriteLine("Message: " + ex.Message);
                Console.WriteLine("\n\nInnerException: " + ex.InnerException);

                Console.ReadKey();
            }
            finally
            {
                client.Close();
            }
        }
    }
}
